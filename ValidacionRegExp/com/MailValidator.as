package com
{
	import mx.validators.RegExpValidator;
	
	public class MailValidator extends RegExpValidator
	{
		public function MailValidator()
		{
			super();
			super.expression = "([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}";
		}
	}
}